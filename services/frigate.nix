{ config, lib, pkgs, ... }:
let
  conf = {
    mqtt.enabled = false;
    ffmpeg.hwaccel_args = "preset-vaapi";
    record = {
      enabled = true;
    };
    cameras = {
      driveway = {
        enabled = true;
        ffmpeg = {
          input_args = "preset-rtsp-restream";
          inputs = [ 
            {
              path = "rtsp://localhost:8554/driveway";
              roles = [ "record" ];
            }
            {
              path = "rtsp://localhost:8554/driveway_sub";
              roles = [ "detect" ];
            }
          ];
        };
        detect.enabled = true;
        review = {
          alerts = {
            required_zones = [ "entire_front" ];
          };
        };
        zones = {
          entire_front = {
            coordinates = "0.133,0.194,0.553,0.269,0.919,0.4,0.982,0.731,0.865,0.984,0.006,0.989,0.009,0.313";
          };
        };
      };
      frontdoor = {
        enabled = true;
        ffmpeg = {
          input_args = "preset-rtsp-restream";
          inputs = [
            {
              path = "rtsp://localhost:8554/frontdoor";
              roles = [ "record" ];
            }
            {
              path = "rtsp://localhost:8554/frontdoor_sub";
              roles = [ "detect" ];
            }
          ];
        };
        detect.enabled = true;
      };
      nursery ={
        enabled = true;
        ffmpeg = {
          input_args = "preset-rtsp-restream";
          inputs = [
            {
              path = "rtsp://localhost:8554/nursery";
              roles = [ "record" ];
            }
            {
              path = "rtsp://localhost:8554/nursery_sub";
              roles = [ "detect" ];
            }
          ];
        };
        detect.enabled = true;
      };
    };
    camera_groups = {
      overview = {
        cameras = [
          "driveway"
          "frontdoor"
          "nursery"
        ];
      icon = "LuCar";
      order = 0;
      };
    };
    auth = {
      session_length = 864000;
      refresh_time = 777600;
    };

    go2rtc = {
      streams = {
        driveway = [
          "rtsp://admin:pNzrurEDPmZpxZ9pKNuTP2aH5@cam-driveway:554/cam/realmonitor?channel=1&subtype=0"
          "ffmpeg:driveway#audio=opus"
        ];
        driveway_sub = [
          "rtsp://admin:pNzrurEDPmZpxZ9pKNuTP2aH5@cam-driveway:554/cam/realmonitor?channel=1&subtype=2"
        ];
        frontdoor = [
          "rtsp://admin:M9vUjabtnsfh5jnJULktUyom9@cam-frontdoor:554/cam/realmonitor?channel=1&subtype=0"
          "ffmpeg:frontdoor#audio=opus"
        ];
        frontdoor_sub = [
          "rtsp://admin:M9vUjabtnsfh5jnJULktUyom9@cam-frontdoor:554/cam/realmonitor?channel=1&subtype=2"
        ];
        nursery = [
          "rtsp://admin:rw9PEWT5wjKm4tyiDcDVcpDx2@cam-nursery:554/cam/realmonitor?channel=1&subtype=0"
          "ffmpeg:nursery#audio=opus"
        ];
        nursery_sub = [
          "rtsp://admin:rw9PEWT5wjKm4tyiDcDVcpDx2@cam-nursery:554/cam/realmonitor?channel=1&subtype=2"
        ];
      };
    };
  };
in
{
  virtualisation.oci-containers.containers = {
    frigate = {
      image = "ghcr.io/blakeblackshear/frigate:stable";
      privileged = true;
      autoStart = true;
      devices = [ "/dev/dri/renderD128:/dev/dri/renderD128" ];
      volumes = [
        "frigate:/config"
        "${(pkgs.formats.yaml {}).generate "frigate.yml" conf}:/config/config.yml"
        "/mnt/hgst-6tb-2017/frigate-storage:/media/frigate"
        "/etc/localtime:/etc/localtime:ro"
      ];
      ports = [
        "80:5000"
        "443:8971"
        "8554:8554" # RTSP feeds
        "8555:8555/tcp" # WebRTC over tcp
        "8555:8555/udp" # WebRTC over udp
        "1984:1984"
      ];
      environment = {
        FRIGATE_RTSP_PASSWORD = "password";
      };
      extraOptions = [
        # "--restart=unless-stopped"
        "--stop-timeout=30"
        "--mount=type=tmpfs,target=/tmp/cache,tmpfs-size=1000000000"
        "--shm-size=512m"
        "--dns=10.0.0.1"
      ];
    };
  }; 
}
