# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, nixos-cosmic, niri, ... }:

let
  json = pkgs.formats.json {};
  pwRnnoiseConfig = {
    "context.modules"= [
      { "name" = "libpipewire-module-filter-chain";
        "args" = {
          "node.description" = "Noise Cancelling Input";
          "media.name"       = "Noise Cancelling Input";
          "filter.graph" = {
            "nodes" = [
              {
                "type"   = "ladspa";
                "name"   = "rnnoise";
                "plugin" = "${pkgs.rnnoise-plugin}/lib/ladspa/librnnoise_ladspa.so";
                "label"  = "noise_suppressor_stereo";
                "control" = {
                  "VAD Threshold (%)" = 80.0;
                };
              }
            ];
          };
          "audio.position" = [ "FL" "FR" ];
          "capture.props" = {
            "node.name" = "effect_input.rnnoise";
            "node.passive" = true;
          };
          "playback.props" = {
            "node.name" = "effect_output.rnnoise";
            "media.class" = "Audio/Source";
          };
        };
      }
    ];
  };
  pwGameOneEQConfig = {
    "context.modules" = [
      { "name" = "libpipewire-module-filter-chain";
        "args" = {
          "node.description" = "Game One EQ Sink";
          "media.name" = "Game One EQ Sink";
          "filter.graph" = {
            "nodes" = [
                {
                  "type" = "builtin";
                  "name" = "eq_preamp";
                  "label" = "bq_highshelf";
                  "control" = { "Freq" = 0; "Q" = 1.0; "Gain" = -3.0; };
                }
                {
                  "type" = "builtin";
                  "name" = "convolver";
                  "label" = "convolver";
                  "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/results_oratory1990_harman_over-ear_2018_Sennheiser_GAME_ONE-48000Hz.wav"; };
                }
              ];
            "links" = [
              { "output" = "eq_preamp:Out"; "input" = "convolver:In"; }
            ];
            "inputs" = [ "eq_preamp:In" ];
            "outputs" = [ "convolver:Out" ];
          };
          "audio.channels" = 2;
          "audio.position" = [ "FL" "FR" ];
          "capture.props" = {
            "node.name" = "effect_input.gameone-convolution-eq";
            "media.class" = "Audio/Sink";
          };
          "playback.props" = {
            "node.name" = "effect_output.gameone-convolution-eq";
            "node.passive" = true;
          };
        };
      }
    ];
  };
  pwGameOneSurroundConfig = {
    "context.modules" = [
      { "name" = "libpipewire-module-filter-chain";
        "args" = {
          "node.description" = "Game One Surround Sink";
          "media.name" = "Game One Surround Sink";
          "filter.graph" = {
            "nodes" = [
                # {
                #   "type" = "builtin";
                #   "name" = "eq_preamp";
                #   "label" = "bq_highshelf";
                #   "control" = { "Freq" = 0; "Q" = 1.0; "Gain" = -6.6; };
                # }
                # {
                #   "type" = "builtin";
                #   "name" = "convolver";
                #   "label" = "convolver";
                #   "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/results_oratory1990_harman_over-ear_2018_Sennheiser_GAME_ONE-48000Hz.wav"; };
                # }
                
                # duplicate inputs
                { "type" = "builtin"; "label" = "copy"; "name" = "copyFL";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyFR";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyFC";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyRL";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyRR";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copySL";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copySR";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyLFE"; }

                # apply hrir - HeSuVi 14-channel WAV (not the *-.wav variants) (note: */44/* in HeSuVi are the same, but resampled to 44100)
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFL_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  0; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFL_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  1; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convSL_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  2; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convSL_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  3; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convRL_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  4; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convRL_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  5; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFC_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  6; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFR_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  7; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFR_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  8; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convSR_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  9; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convSR_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 10; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convRR_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 11; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convRR_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 12; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFC_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 13; }; }

                # treat LFE as FC
                { "type" = "builtin"; "label" = "convolver"; "name" = "convLFE_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  6; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convLFE_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 13; }; }

                # stereo output
                { "type" = "builtin"; "label" = "mixer"; "name" = "mixL"; }
                { "type" = "builtin"; "label" = "mixer"; "name" = "mixR"; }
              ];
            "links" = [
              # input
              { "output" = "copyFL:Out"; "input" = "convFL_L:In"; }
              { "output" = "copyFL:Out"; "input" = "convFL_R:In"; }
              { "output" = "copySL:Out"; "input" = "convSL_L:In"; }
              { "output" = "copySL:Out"; "input" = "convSL_R:In"; }
              { "output" = "copyRL:Out"; "input" = "convRL_L:In"; }
              { "output" = "copyRL:Out"; "input" = "convRL_R:In"; }
              { "output" = "copyFC:Out"; "input" = "convFC_L:In"; }
              { "output" = "copyFR:Out"; "input" = "convFR_R:In"; }
              { "output" = "copyFR:Out"; "input" = "convFR_L:In"; }
              { "output" = "copySR:Out"; "input" = "convSR_R:In"; }
              { "output" = "copySR:Out"; "input" = "convSR_L:In"; }
              { "output" = "copyRR:Out"; "input" = "convRR_R:In"; }
              { "output" = "copyRR:Out"; "input" = "convRR_L:In"; }
              { "output" = "copyFC:Out"; "input" = "convFC_R:In"; }
              { "output" = "copyLFE:Out"; "input" = "convLFE_L:In"; }
              { "output" = "copyLFE:Out"; "input" = "convLFE_R:In"; }

              # output
              { "output" = "convFL_L:Out"; "input" = "mixL:In 1"; }
              { "output" = "convFL_R:Out"; "input" = "mixR:In 1"; }
              { "output" = "convSL_L:Out"; "input" = "mixL:In 2"; }
              { "output" = "convSL_R:Out"; "input" = "mixR:In 2"; }
              { "output" = "convRL_L:Out"; "input" = "mixL:In 3"; }
              { "output" = "convRL_R:Out"; "input" = "mixR:In 3"; }
              { "output" = "convFC_L:Out"; "input" = "mixL:In 4"; }
              { "output" = "convFC_R:Out"; "input" = "mixR:In 4"; }
              { "output" = "convFR_R:Out"; "input" = "mixR:In 5"; }
              { "output" = "convFR_L:Out"; "input" = "mixL:In 5"; }
              { "output" = "convSR_R:Out"; "input" = "mixR:In 6"; }
              { "output" = "convSR_L:Out"; "input" = "mixL:In 6"; }
              { "output" = "convRR_R:Out"; "input" = "mixR:In 7"; }
              { "output" = "convRR_L:Out"; "input" = "mixL:In 7"; }
              { "output" = "convLFE_R:Out"; "input" = "mixR:In 8"; }
              { "output" = "convLFE_L:Out"; "input" = "mixL:In 8"; }

              # EQ
              # { "output" = "eq_preamp:Out"; "input" = "convolver:In"; }
            ];
            # "inputs" = [ "eq_preamp:In" ];
            # "outputs" = [ "convolver:Out" ];
            "inputs"  = [ "copyFL:In" "copyFR:In" "copyFC:In" "copyLFE:In" "copyRL:In" "copyRR:In" "copySL:In" "copySR:In" ];
            "outputs" = [ "mixL:Out" "mixR:Out" ];
          };
          "capture.props" = {
            "node.name" = "effect_input.gameone-surround";
            "media.class" = "Audio/Sink";
            "audio.channels" = 8;
            "audio.position" = [ "FL" "FR" "FC" "LFE" "RL" "RR" "SL" "SR" ];
          };
          "playback.props" = {
            "node.name" = "effect_output.gameone-surround";
            "node.passive" = true;
            "audio.channels" = 2;
            "audio.position" = [ "FL" "FR" ];
          };
        };
      }
    ];
  };
pwVirtualSurroundConfig = {
  "context.modules" = [
    { "name" = "libpipewire-module-filter-chain";
      "args" = {
        "node.description" = "Virtual Surround Sink";
        "media.name" = "Virtual Surround Sink";
        "filter.graph" = {
          "nodes" = [
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spFL";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 30.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spFR";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 330.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spFC";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 0.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spRL";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 150.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spRR";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 210.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spSL";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 90.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spSR";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 270.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spLFE";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 0.0; "Elevation" = -6.0; "Radius" = 3.0; };
            }

            # Downmix to 2 channels
            { "type" = "builtin"; "label" = "mixer"; "name" = "mixL"; }
            { "type" = "builtin"; "label" = "mixer"; "name" = "mixR"; }
          ];
          links = [
            # output
            { "output" = "spFL:Out L"; "input" = "mixL:In 1"; }
            { "output" = "spFL:Out R"; "input" = "mixR:In 1"; }
            { "output" = "spFR:Out L"; "input" = "mixL:In 2"; }
            { "output" = "spFR:Out R"; "input" = "mixR:In 2"; }
            { "output" = "spFC:Out L"; "input" = "mixL:In 3"; }
            { "output" = "spFC:Out R"; "input" = "mixR:In 3"; }
            { "output" = "spRL:Out L"; "input" = "mixL:In 4"; }
            { "output" = "spRL:Out R"; "input" = "mixR:In 4"; }
            { "output" = "spRR:Out L"; "input" = "mixL:In 5"; }
            { "output" = "spRR:Out R"; "input" = "mixR:In 5"; }
            { "output" = "spSL:Out L"; "input" = "mixL:In 6"; }
            { "output" = "spSL:Out R"; "input" = "mixR:In 6"; }
            { "output" = "spSR:Out L"; "input" = "mixL:In 7"; }
            { "output" = "spSR:Out R"; "input" = "mixR:In 7"; }
            { "output" = "spLFE:Out L"; "input" = "mixL:In 8"; }
            { "output" = "spLFE:Out R"; "input" = "mixR:In 8"; }
          ];
          "inputs" = [ "spFL:In" "spFR:In" "spFC:In" "spLFE:In" "spRL:In" "spRR:In" "spSL:In" "spSR:In" ];
          "outputs" = [ "mixL:Out" "mixR:Out" ];
        };
        "capture.props" = {
          "node.name" = "effect_input.spatializer";
          "media.class" = "Audio/Sink";
          "audio.channels" = 8;
          "audio.position" = [ "FL" "FR" "FC" "LFE" "RL" "RR" "SL" "SR" ];
        };
        "playback.props" = {
          "node.name" = "effect_output.spatializer";
          "node.passive" = "true";
          "audio.channels" = 2;
          "audio.position" = [ "FL" "FR" ];
        };
      };
    }
  ];
};
in
{
  nixpkgs.overlays = [ niri.overlays.niri ];

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix

      ../services/frigate.nix      
    ];

  # Additional file system configuration
  swapDevices = [{ device = "/swap/swapfile"; }];
  fileSystems = {
    "/".options = [ "compress=lzo" ];
    "/mnt/samsung-120gb".options = [ "compress=zstd:1"];
    "/mnt/wd-1tb".options = [ "compress=zstd:1" ];
    "/mnt/hgst-6tb-2017".options = [ "compress=zstd:3" ];
  };
  
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Linux kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.extraModulePackages = [
    config.boot.kernelPackages.v4l2loopback
    config.boot.kernelPackages.ddcci-driver
  ];
  boot.kernelModules = [ "i2c-dev" "ddcci_backlight" ];

  # Virtualization
  virtualisation = {
    libvirtd.enable = true;
    podman = {
      enable = true;
      defaultNetwork.settings.dns_enabled = true;
      # dockerCompat = true;
    };
  };
  
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  # Network configuration
  
  networking = {
    hostName = "apollo";
    useDHCP = false;
    networkmanager = {
      enable = true;
    };
    interfaces = {
      enp5s0.useDHCP = true;
      wlp6s0.useDHCP = true;
    };
    extraHosts = ''
      10.72.0.34 account-dev.carc.usc.edu hpcaccount-dev.usc.edu
    '';
    # Open ports in the firewall.
    firewall.allowedTCPPorts = [ 
      80
      5900 # VNC
      3333 # lbry
    ];
    firewall.trustedInterfaces = [ 
      "podman3" # Allow podman containers access
    ];
    firewall.allowedUDPPorts = [
      4444 # lbry
    ];
    # Or disable the firewall altogether.
    # firewall.enable = false;
  };

  services.tailscale = {
    enable = true;
    authKeyFile = "/home/bill/.config/tailscale/tailscale.key";
  };

  # Use systemd.resolved, it works better in conjunction with VPN connections
  services.resolved.enable = true;

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  # networking.useDHCP = false;
  # networking.interfaces.enp4s0.useDHCP = true;
  # networking.interfaces.wlp5s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Configuration to enable dnscrypts-proxy2
  # networking.nameservers = [ "127.0.0.1" "::1" ];
  # networking.resolvconf.enable = false;
  # networking.dhcpcd.extraConfig = "nohook resolve.conf";
  # networking.networkmanager.dns = "none";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
    inputMethod = {
      enable = true;
      type = "fcitx5";
      fcitx5 = {
        waylandFrontend = true;
        plasma6Support = config.services.desktopManager.plasma6.enable;
        addons = [ pkgs.fcitx5-rime ];
      };
    };
  };
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # TimeSync configuration
  services.timesyncd.extraConfig = ''
    PollIntervalMaxSec=180
  '';

  # Desktop Environments

  # Use dbus-broker for better compatibility and performance
  services.dbus.implementation = "broker";

  # Enable the Plasma 6 Desktop Environment.
  services.xserver.enable = true;
  services.xserver.dpi = 109;
  # services.displayManager.sddm.enable = true;
  services.desktopManager.plasma6 = {
    enable = false;
    enableQt5Integration = true;
  };
  # hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.legacy_390;
  hardware.graphics.enable = true;
  hardware.graphics.enable32Bit = true;
  security.pam.services.kdewallet.enableKwallet = true;

  # Enable Cosmic Desktop Environment
  services.desktopManager.cosmic.enable = true;
  services.displayManager.cosmic-greeter.enable = true;

  # Enable Cosmic Niri
  services.displayManager.sessionPackages = [
    ((
        pkgs.writeTextFile {
          name = "cosmic-on-niri";
          destination = "/share/wayland-sessions/COSMIC-on-niri.desktop";
          text = ''
            [Desktop Entry]
            Name=COSMIC-on-niri
            Comment=This session logs you into the COSMIC desktop on niri
            Type=Application
            DesktopNames=niri
            Exec=${pkgs.writeShellApplication {
              name = "start-cosmic-ext-niri";
              runtimeInputs = [pkgs.systemd pkgs.dbus nixos-cosmic.packages.x86_64-linux.cosmic-session pkgs.bash pkgs.coreutils];
              text = ''
                set -e

                # From: https://people.debian.org/~mpitt/systemd.conf-2016-graphical-session.pdf

                if command -v systemctl >/dev/null; then
                    # robustness: if the previous graphical session left some failed units,
                    # reset them so that they don't break this startup
                    for unit in $(systemctl --user --no-legend --state=failed --plain list-units | cut -f1 -d' '); do
                        partof="$(systemctl --user show -p PartOf --value "$unit")"
                        for target in cosmic-session.target graphical-session.target; do
                            if [ "$partof" = "$target" ]; then
                                systemctl --user reset-failed "$unit"
                                break
                            fi
                        done
                    done
                fi

                # use the user's preferred shell to acquire environment variables
                # see: https://github.com/pop-os/cosmic-session/issues/23
                if [ -n "''${SHELL:-}" ]; then
                    # --in-login-shell: our flag to indicate that we don't need to recurse any further
                    if [ "''${1:-}" != "--in-login-shell" ]; then
                        # `exec -l`: like `login`, prefixes $SHELL with a hyphen to start a login shell
                        exec bash -c "exec -l ${"'"}''${SHELL}' -c ${"'"}''${0} --in-login-shell'"
                    fi
                fi

                export XDG_CURRENT_DESKTOP="''${XDG_CURRENT_DESKTOP:=niri}"
                export XDG_SESSION_TYPE="''${XDG_SESSION_TYPE:=wayland}"
                export XCURSOR_THEME="''${XCURSOR_THEME:=Cosmic}"
                export _JAVA_AWT_WM_NONREPARENTING=1
                export GDK_BACKEND=wayland,x11
                export MOZ_ENABLE_WAYLAND=1
                export QT_QPA_PLATFORM="wayland;xcb"
                export QT_AUTO_SCREEN_SCALE_FACTOR=1
                export QT_ENABLE_HIGHDPI_SCALING=1
                export GTK_IM_MODULE=fcitx
                export QT_IM_MODULE=fcitx
                export SDL_IM_MODULE=fcitx

                if command -v systemctl >/dev/null; then
                    # set environment variables for new units started by user service manager
                    # systemctl --user import-environment XDG_SESSION_TYPE XDG_CURRENT_DESKTOP WAYLAND_DISPLAY
                    systemctl --user import-environment
                    dbus-update-activation-environment --all
                fi
                # Run cosmic-session
                if [[ -z "''${DBUS_SESSION_BUS_ADDRESS}" ]]; then
                    exec dbus-run-session -- cosmic-session niri --session
                else
                    exec cosmic-session niri --session
                fi
              '';
            }}/bin/start-cosmic-ext-niri
          '';
        }
      )
      .overrideAttrs
      (old: {
        passthru.providedSessions = ["COSMIC-on-niri"];
      })
    )
  ];

  # Other Desktop Environment configuration
  xdg.portal = {
    enable = true;
    xdgOpenUsePortal = true;
    wlr.enable = true;
  };

  # Configure fonts
  fonts = {
    packages = with pkgs; [
      sudo-font
      mplus-outline-fonts.githubRelease
      google-fonts
      source-sans
      source-han-sans
      source-serif
      source-han-serif
      lmodern
      league-of-moveable-type
      recursive
    ]
    ++ builtins.filter lib.attrsets.isDerivation (builtins.attrValues pkgs.nerd-fonts);

    fontconfig.defaultFonts = {
      monospace = lib.mkForce [ "Rec Mono Duotone" "Noto Sans Mono CJK TC" "Noto Sans Mono CJK SC" ];
      sansSerif = lib.mkForce [ "Source Sans 3" "Source Han Sans TC" "Source Han Sans SC" ];
      serif     = lib.mkForce [ "Source Serif 4" "Source Han Serif TC" "Source Han Serif SC" ];
    };
  };
  
  # Configure keymap in X11
  services.xserver.xkb.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = with pkgs; [
      gutenprint
      gutenprintBin
      foomatic-filters
    ];
  };

  # Hardware configuration
  hardware = {
    sane.enable = true;
    i2c.enable = true;
    opentabletdriver = {
      enable = true;
      blacklistedKernelModules = [ "hid-uclogic" ];
      daemon.enable = false;
    };
  };
  
  # Enable Bluetooth
  hardware.bluetooth = {
    enable = true;
    settings ={
      General = { Experimental = true; };
    };
  };

  # Enable sound.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
    extraConfig = {
      pipewire = {
        # Generate PipeWire noise suppression config
        "99-input-rnnoise.conf" = pwRnnoiseConfig;

        # Generate PipeWire Game One EQ config
        "100-sink-gameone-eq.conf" = pwGameOneEQConfig;

        # Generate PipeWire Game One virtual surround config
        "101-sink-gameone-surround.conf" = pwGameOneSurroundConfig;

        # Generate PipeWire virtual surround config
        "102-sink-surround.conf" = pwVirtualSurroundConfig;
      };
    };
  };
  
  # Enable monitor brightness control
  services.ddccontrol.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = { 
    defaultUserShell = pkgs.fish;
    users.bill = {
      description = "Bill Kuo-Shin Sun";
      isNormalUser = true;
      extraGroups = [
        "wheel"
        "networkmanager"
        "syncthing"
        "scanner"
        "lp"
        "kvm"
        "libvirtd"
        "adbusers"
        "dialout"
        "video"
        "input"
      ];
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [ "olm-3.2.16" ];
  environment.systemPackages = with pkgs; [
    wl-clipboard
    wget
    ntfs3g
    home-manager
    lm_sensors
    hdparm
    git
    fzf
    fd
    bat
    fishPlugins.fzf-fish
    fishPlugins.done
    podman-compose
  ];
  
  # Environment
  environment = {
    etc.hosts.mode = "0644";
    sessionVariables = {
      # The following variables are set to make fcitx input method switching work in wayland
      NIX_PROFILES =
        "${lib.strings.concatStringsSep " " (lib.lists.reverseList config.environment.profiles)}";
      XMODIFIERS = "@im=fcitx";

      # Hint electron apps to use wayland
      NIXOS_OZONE_WL = "1";
    };
  };
  
  # Nix configuration.
  nix = {
    settings = {
      substituters= [
        "https://cache.nixos.org" "https://nixcache.reflex-frp.org"
        "https://cache.iog.io"
      ];
      trusted-public-keys = [
        "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="
        "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
      ];
      trusted-users = [ "root" "bill" ];
    };    
    gc.automatic = false;
    # gc.options = "--delete-older-than 30d";
    extraOptions = ''
      auto-optimise-store = true
      keep-outputs = true
      experimental-features = nix-command flakes
    '';
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  programs = {
    niri = {
      enable = true;
      package = pkgs.niri-unstable;
    };
    
    fish.enable = true;
    firefox.enable = true;
    ssh.startAgent = true;
    partition-manager.enable = true;
    kdeconnect.enable = true;
    
    corectrl = {
      enable = true;
      gpuOverclock.enable = true;
    };
    
    steam = {
      enable = true;
      remotePlay.openFirewall = true;
      gamescopeSession.enable = true;
    };

    gamemode.enable = true;
    
    weylus = {
      enable = true;
      openFirewall = true;
    };
    
    singularity = {
      enable = true;
      package = pkgs.apptainer;
    };
  };

  # List services that you want to enable:
  
  # Enable one-time service to create btrfs swap file
  # systemd.services = {
  #   create-swapfile = {
  #     serviceConfig.Type = "oneshot";
  #     wantedBy = [ "swap-swapfile.swap" ];
  #     script = ''
  #       ${pkgs.coreutils}/bin/truncate -s 0 /swap/swapfile
  #       ${pkgs.e2fsprogs}/bin/chattr +C /swap/swapfile
  #       ${pkgs.btrfs-progs}/bin/btrfs property set /swap/swapfile compression none
  #     '';
  #   };
  # };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  services.fwupd.enable = true;

  # Enable Avahi
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    publish.userServices = true;
  };

  # Enable flatpak
  services.flatpak.enable = true;

  # Enable backup
  services.restic.backups = {
    local = {
      initialize = true;
      paths = [ "/home/" "/mnt/wd-1tb/Videos/" ];
      repository = "/mnt/hgst-6tb-2017/Backups/";
      passwordFile = "/etc/nixos/secrets/local-backup";
      pruneOpts = [
        "--keep-daily 14"
        "--keep-weekly 13"
      ];
    };
    # remote = {
    #   initialize = true;
    #   paths = [ "/home/" ];
    #   repository = "rclone:remote-b2:backup-bill/Repository";
    #   passwordFile = "/etc/nixos/secrets/remote-backup";
    #   rcloneConfigFile = "/home/bill/.config/rclone/rclone.conf";
    #   timerConfig = {
    #     OnCalendar = "weekly";
    #   };
    #   pruneOpts = [
    #     "--keep-daily 14"
    #     "--keep-weekly 13"
    #   ];
    # };
  };

  # Enable OpenRGB
  services.hardware.openrgb = {
    enable = true;
    motherboard = "amd";
  };

  services.sunshine = {
    enable = true;
    openFirewall = true;
    capSysAdmin = true;
  };

  services.home-assistant = {
    enable = true;
    extraComponents = [
      # Components required to complete the onboarding
      "esphome"
      "met"
      "radio_browser"
      # Recommended for fast zlib compression
      # https://www.home-assistant.io/integrations/isal
      "isal"
    ];
    customComponents = with pkgs.home-assistant-custom-components; [
      frigate
    ];
    config = {
      # Includes dependencies for a basic setup
      # https://www.home-assistant.io/integrations/default_config/
      default_config = {};
    };
  };

  # Enable extra udev rules for Atreus
  services.udev.packages = [ pkgs.chrysalis ];

  # Enable GitLab self-hosted runners
  services.gitlab-runner = {
    enable = true;
    services = {
      # runner for building in docker via host's nix-daemon
      # nix store will be readable in runner, might be insecure
      nix = {
        # File should contain at least these two variables:
        # `CI_SERVER_URL`
        # `CI_SERVER_TOKEN`
        registrationConfigFile = "/etc/nixos/secrets/gitlab-runner-nix-registration";
        dockerImage = "alpine";
        dockerVolumes = [
          "/nix/store:/nix/store:ro"
          "/nix/var/nix/db:/nix/var/nix/db:ro"
          "/nix/var/nix/daemon-socket:/nix/var/nix/daemon-socket:ro"
        ];
        dockerDisableCache = true;
        preBuildScript = pkgs.writeScript "setup-container" ''
          mkdir -p -m 0755 /nix/var/log/nix/drvs
          mkdir -p -m 0755 /nix/var/nix/gcroots
          mkdir -p -m 0755 /nix/var/nix/profiles
          mkdir -p -m 0755 /nix/var/nix/temproots
          mkdir -p -m 0755 /nix/var/nix/userpool
          mkdir -p -m 1777 /nix/var/nix/gcroots/per-user
          mkdir -p -m 1777 /nix/var/nix/profiles/per-user
          mkdir -p -m 0755 /nix/var/nix/profiles/per-user/root
          mkdir -p -m 0700 "$HOME/.nix-defexpr"

          . ${pkgs.nix}/etc/profile.d/nix.sh

          ${pkgs.nix}/bin/nix-env -i ${lib.concatStringsSep " " (with pkgs; [ nix cacert git openssh ])}

          ${pkgs.nix}/bin/nix-channel --add https://nixos.org/channels/nixpkgs-unstable
          ${pkgs.nix}/bin/nix-channel --update nixpkgs
        '';
        environmentVariables = {
          ENV = "/etc/profile";
          USER = "root";
          NIX_REMOTE = "daemon";
          PATH = "/nix/var/nix/profiles/default/bin:/nix/var/nix/profiles/default/sbin:/bin:/sbin:/usr/bin:/usr/sbin";
          NIX_SSL_CERT_FILE = "/nix/var/nix/profiles/default/etc/ssl/certs/ca-bundle.crt";
        };
      };
      # runner for building docker images
      docker-images = {
        # File should contain at least these two variables:
        # `CI_SERVER_URL`
        # `CI_SERVER_TOKEN`
        registrationConfigFile = "/etc/nixos/secrets/gitlab-runner-docker-images-registration";
        dockerImage = "docker:stable";
        dockerVolumes = [
          "/var/run/docker.sock:/var/run/docker.sock"
        ];
      };
      # runner for everything else
      default = {
        # File should contain at least these two variables:
        # `CI_SERVER_URL`
        # `CI_SERVER_TOKEN`
        registrationConfigFile = "/etc/nixos/secrets/gitlab-runner-default-registration";
        dockerImage = "debian:stable";
      };
    };
  };

  services.slurm = {
    server.enable = true;
    client.enable = true;
    nodeName = [
      "apollo CPUs=1 State=UNKNOWN"
    ];
    partitionName = [
      "main Nodes=apollo Default=YES MaxTime=INFINITE State=UP"
    ];
    extraConfig = ''
      SlurmctldHost=apollo

      AuthAltTypes=auth/jwt
      AuthAltParameters=jwt_key=/var/spool/slurmctld/jwt_hs256.key
      AccountingStorageType=accounting_storage/slurmdbd
    '';
    dbdserver = {
      enable = true;
      storagePassFile = "/var/spool/slurmctld/storage_pass";
      extraConfig = ''
        AuthAltTypes=auth/jwt
        AuthAltParameters=jwt_key=/var/spool/slurmctld/jwt_hs256.key
      '';
    };
  };
  services.mysql = {
    enable = true;
    package = pkgs.mariadb;
    ensureUsers = 
    [
      {
        name = "bill";
        ensurePermissions = {
          "*.*" = "ALL PRIVILEGES";
        };
      }
      {
        name = "slurm";
        ensurePermissions = {
          "slurm_acct_db.*" = "ALL PRIVILEGES";
        };
      }
    ];
    ensureDatabases = [ "slurm_acct_db" ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}

