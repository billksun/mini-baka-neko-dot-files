{ extensions = [
  {
    name = "vscode-languagetool";
    publisher = "adamvoss";
    version = "3.8.0";
    sha256 = "1p6cjb61509id66lynfshzdsvw43acnrhsm0h0g56zr65gz8nn9i";
  }
  {
    name = "vscode-languagetool-en";
    publisher = "adamvoss";
    version = "3.8.0";
    sha256 = "06mll2kz89kw3r4xr2p422isn85ifi9b6bsy8xi1x37mg3rw3b8r";
  }
  {
    name = "ng-template";
    publisher = "Angular";
    version = "13.3.0";
    sha256 = "0blnkyhcpp76i349gxh259559xr2pdlpac3xdrsmw2c17rd3afik";
  }
  {
    name = "nix-env-selector";
    publisher = "arrterian";
    version = "1.0.7";
    sha256 = "0mralimyzhyp4x9q98x3ck64ifbjqdp8cxcami7clvdvkmf8hxhf";
  }
  {
    name = "asciidoctor-vscode";
    publisher = "asciidoctor";
    version = "2.9.8";
    sha256 = "0wj6gixg197wrhmryi0kxj53w0qngmhgpw5aklriq858ms4c0zf3";
  }
  {
    name = "vscode-tailwindcss";
    publisher = "bradlc";
    version = "0.7.7";
    sha256 = "08ghx6y5rxfnzxlpb623gb70qhiwa3dqgmy8zac1ywh4nwmi64x9";
  }
  {
    name = "path-intellisense";
    publisher = "christian-kohler";
    version = "2.8.0";
    sha256 = "04vardis9k6yzaha5hhhv16c3z6np48adih46xj88y83ipvg5z2l";
  }
  {
    name = "vscode-markdownlint";
    publisher = "DavidAnson";
    version = "0.47.0";
    sha256 = "0v50qcfs3jx0m2wqg4qbhw065qzdi57xrzcwnhcpjhg1raiwkl1a";
  }
  {
    name = "gitlens";
    publisher = "eamodio";
    version = "12.0.5";
    sha256 = "0zfawv9nn88x8m30h7ryax0c7p68najl23a51r88a70hqppzxshw";
  }
  {
    name = "vscode-firefox-debug";
    publisher = "firefox-devtools";
    version = "2.9.6";
    sha256 = "0mx70gyigz26x56r01vvpdrvgs10zzqzv204jbkjzfwh4za45zv7";
  }
  {
    name = "vscode-pull-request-github";
    publisher = "GitHub";
    version = "0.41.2022041209";
    sha256 = "1qx00ac9b0v1137705dv3df78c8pnbx8sp79n377fxqlqv30iwsr";
  }
  {
    name = "haskell";
    publisher = "haskell";
    version = "2.0.0";
    sha256 = "1q98klx5gp7q5i7in9c31hh5bn62h4agi3p6jw6bns48f95wwrvn";
  }
  {
    name = "haskell-linter";
    publisher = "hoovercj";
    version = "0.0.6";
    sha256 = "0fb71cbjx1pyrjhi5ak29wj23b874b5hqjbh68njs61vkr3jlf1j";
  }
  {
    name = "latex-workshop";
    publisher = "James-Yu";
    version = "8.24.1";
    sha256 = "075ym4f1ajfaxnpyvqi0jwk3079lng1qnr24hhpw3z2yd433vx4i";
  }
  {
    name = "nix-ide";
    publisher = "jnoortheen";
    version = "0.1.20";
    sha256 = "16mmivdssjky11gmih7zp99d41m09r0ii43n17d4i6xwivagi9a3";
  }
  {
    name = "language-haskell";
    publisher = "justusadam";
    version = "3.6.0";
    sha256 = "115y86w6n2bi33g1xh6ipz92jz5797d3d00mr4k8dv5fz76d35dd";
  }
  {
    name = "ledger";
    publisher = "mariosangiorgio";
    version = "1.0.9";
    sha256 = "1lyhldyl4f1c2hxqwn6arma8p1snm2cbrdr1vybxllrh570lcgz9";
  }
  {
    name = "rainbow-csv";
    publisher = "mechatroner";
    version = "2.2.0";
    sha256 = "0z5icnhp45spmpgjvz7icrcig2r8dzww3msr6a9danapwj6cvcvz";
  }
  {
    name = "python";
    publisher = "ms-python";
    version = "2022.5.11021002";
    sha256 = "04ibdn3iqx5c2vbzx1svqkhx634hpzds9lchi9hvvjvskahzgcyz";
  }
  {
    name = "vscode-pylance";
    publisher = "ms-python";
    version = "2022.4.0";
    sha256 = "1vlldyhlxzwm6c35wa3r3khh8bc7xvmddm5vw4yh5v8yvdny019b";
  }
  {
    name = "jupyter";
    publisher = "ms-toolsai";
    version = "2022.4.1001041007";
    sha256 = "0vmc9lxys26mddkcyz89pr5wygjma82zn3lwksfshzgn2plz09im";
  }
  {
    name = "jupyter-keymap";
    publisher = "ms-toolsai";
    version = "1.0.0";
    sha256 = "0wkwllghadil9hk6zamh9brhgn539yhz6dlr97bzf9szyd36dzv8";
  }
  {
    name = "jupyter-renderers";
    publisher = "ms-toolsai";
    version = "1.0.6";
    sha256 = "0sb3ngpl4skylbmz7zbj7s79xala29wrgn1c3m4agp00ixz451fq";
  }
  {
    name = "vscode-typescript-tslint-plugin";
    publisher = "ms-vscode";
    version = "1.3.4";
    sha256 = "0zbg99x71scpgdyicp7fryxmg51fj2fy0dmfm04zq26s0g0n6gn1";
  }
  {
    name = "wordcount";
    publisher = "ms-vscode";
    version = "0.1.0";
    sha256 = "164s721bqbw2lh770vli9vij8q79033nd5k1acxwadmlf99hmgj1";
  }
  {
    name = "vscode-paste-image";
    publisher = "mushan";
    version = "1.0.4";
    sha256 = "1wkplvrn31vly5gw35hlgpjpxgq3dzb16hz64xcf77bwcqfnpakb";
  }
  {
    name = "haskell-ghcid";
    publisher = "ndmitchell";
    version = "0.3.1";
    sha256 = "1rivzlk32x7vq84ri426nhd6a4nv3h7zp7xcsq31d0kp8bqczvi9";
  }
  {
    name = "ide-purescript";
    publisher = "nwolverson";
    version = "0.25.12";
    sha256 = "1f9064w18wwp3iy8ciajad8vlshnzyhnqy8h516k0j5bflz781mn";
  }
  {
    name = "language-purescript";
    publisher = "nwolverson";
    version = "0.2.8";
    sha256 = "1nhzvjwxld53mlaflf8idyjj18r1dzdys9ygy86095g7gc4b1qys";
  }
  {
    name = "vscode-versionlens";
    publisher = "pflannery";
    version = "1.0.9";
    sha256 = "1cym3x36a3lpysqfwxmqf9iwykjf72l8f3s8a3ayl2m7njg15wbh";
  }
  {
    name = "vscode-gitignore-generator";
    publisher = "piotrpalarz";
    version = "1.0.3";
    sha256 = "0yf3h7hd2vx8ic8fgmphad2al3d9w7a9vxis63nwd4fphn9678vs";
  }
  {
    name = "vscode-yaml";
    publisher = "redhat";
    version = "1.6.0";
    sha256 = "123ycy1lky6gcz22gp2zc0y2wiyq97v85p2dxhdsm5dqf6bxjz1q";
  }
  {
    name = "vscode-hsx";
    publisher = "s0kil";
    version = "0.4.0";
    sha256 = "10xfkh9ycg6zd8vamwsrval271y8wln04zhy83x3daq1qkwp4r7x";
  }
  {
    name = "markdown-preview-enhanced";
    publisher = "shd101wyy";
    version = "0.6.2";
    sha256 = "02jilbx7zr1rg7ss793wm5wm1s7kfspdk2y8hrv7gsqjhl15fd5n";
  }
  {
    name = "code-spell-checker";
    publisher = "streetsidesoftware";
    version = "2.1.11";
    sha256 = "0zjvv6msz9w9k81rkynqp6xgfzd11slakmr1rm8v875bpgzdfg9s";
  }
  {
    name = "markdown-memo";
    publisher = "svsool";
    version = "0.3.18";
    sha256 = "024v54qqv8kgxv2bm8wfi64aci5xm4cry2b0z8xr322mgma2m5na";
  }
  {
    name = "markdown-links";
    publisher = "tchayen";
    version = "0.8.0";
    sha256 = "08wchwz4wi8429qxhzdc7x7q9nvx1ykph88hccrn0irqsycm2z32";
  }
  {
    name = "org-mode";
    publisher = "vscode-org-mode";
    version = "1.0.0";
    sha256 = "1dp6mz1rb8awrrpig1j8y6nyln0186gkmrflfr8hahaqr668il53";
  }
  {
    name = "markdown-all-in-one";
    publisher = "yzhang";
    version = "3.4.0";
    sha256 = "0ihfrsg2sc8d441a2lkc453zbw1jcpadmmkbkaf42x9b9cipd5qb";
  }
];
}