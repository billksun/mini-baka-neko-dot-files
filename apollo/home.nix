{ pkgs, lib, config, cosmic-ext-alternative-startup, yazi-flavors, ... }:

with pkgs;

let
  vscodeExtensionsFromMarketplace = import ./vscode/extensions.nix;
in
{
  manual.manpages.enable = false;
  home.username = "bill";
  home.homeDirectory = "/home/bill";
  home.sessionPath = [ "~/.local/bin" ];
  home.sessionVariables = { 
    EDITOR = "hx";
  };
  home.stateVersion = "22.11";

  qt = {
    enable = true;
    platformTheme.name = "kde";
    style.name = "breeze";
  };
  
  gtk = {
    enable = true;
    theme = {
      package = pkgs.gnome-themes-extra;
      name = "Adwaita";
    };
    iconTheme = {
      package = pkgs.adwaita-icon-theme;
      name = "Adwaita";
    };
  };
  
  home.packages = with pkgs; [
    # Utilities

    lm_sensors
    smartmontools
    wayland-utils
    vulkan-tools
    glxinfo
    ripgrep
    pass
    openconnect
    python3
    xorg.xmessage
    unzip
    # unar
    p7zip
    imagemagick
    tesseract
    qpdf
    texlive.combined.scheme-full
    tectonic
    pandoc
    mermaid-filter
    plantuml
    pandoc-plantuml-filter
    nuspell
    aspell
    aspellDicts.en
    hunspellDicts.en_US
    xdotool
    xclip
    restic
    gnome-network-displays
    virt-manager
    ark
    appimage-run
    ltex-ls
    marksman
    ookla-speedtest

    # For custom desktop environment
    # blueman
    # networkmanagerapplet
    # kdePackages.polkit-kde-agent
    wluma
    brightnessctl
    xwayland-satellite

    # Applications

    gparted
    chrysalis
    kcolorchooser
    ktimetracker
    skanlite
    kdePackages.skanpage
    simple-scan
    ktorrent
    digikam
    gwenview
    krita
    inkscape
    kate
    # logseq
    # zettlr
    anytype
    okular
    webcord
    slack
    # neochat
    signal-desktop
    # jami-daemon
    # jami-client-qt
    keepassxc
    _1password-gui
    google-chrome
    libreoffice-qt
    thunderbird
    # kalendar
    # kdePackages.kdepim-addons
    # kdePackages.akonadiconsole
    kdePackages.krdc
    kdePackages.krfb
    kdePackages.filelight
    # kdePackages.kasts
    vlc
    clapper
    haruna
    elisa
    kdePackages.plasmatube
    # zotero
    hledger
    hledger-ui
    hledger-web
    # haskellPackages.hledger-flow
    pencil
    # zoom-us
    dbeaver-bin
    beekeeper-studio
    exercism
    phoronix-test-suite
    lbry
    haskellPackages.patat

    # Games

    lutris
    wine-wayland
    protontricks
    openssl # Lutris' game installer requires this
    zenity # Lutris' game installer requires this
    heroic # Epic Games Launcher

    # Runtimes

    jre

    # Development tools
    
    nil
    kdiff3
    cachix
    nix-prefetch-git
    patchelf
    arduino
  ]; 

  programs.home-manager.enable = true;

# Custom desktop environment
  programs.niri = {
    package = pkgs.niri;
    settings = {
      input = {
        touchpad = {
          accel-speed = 0.6;
        };
      };
      workspaces."Home" = {};
      workspaces."Work" = {};
      workspaces."Code" = {};
      workspaces."Play" = {};
      workspaces."Comm" = {
        open-on-output = "PNP(YUK) REALTEK 8R33926O00QS";
      };
      environment = {
        DISPLAY = ":1";
      };
      spawn-at-startup = [
        { command = [ "${cosmic-ext-alternative-startup.packages.x86_64-linux.default}/bin/cosmic-ext-alternative-startup" ];}
        { command = [ "wluma" ];}
        # { command = [ "cosmic-panel" ];}
        { command = [ "slack" ];}
        { command = [ "thunderbird" ];}
        { command = [ "webcord" ];}
        { command = [ "firefox" ];}
        { command = [ "keepassxc" ];}
        { command = [ "xwayland-satellite" ];}
      ];
      window-rules = [
        {
          matches = [
            {
              app-id = "firefox";
              title = "^\[Work\].*$";
              is-active-in-column = true;
            }
          ];
          open-on-workspace = "Work";
        }
        {
          matches = [
            {
              app-id = "firefox";
              title = "^\[Code\].*$";
              is-active-in-column = true;
            }
          ];
          open-on-workspace = "Comm";
        }
        {
          matches = [
            {
              app-id = "firefox";
              title = "^\[Play\].*$";
              is-active-in-column = true;
            }
          ];
          open-on-workspace = "Play";
        }
        {
          matches = [
            { app-id = "Slack"; }
            { app-id = "thunderbird"; }
            { app-id = "WebCord"; }
          ];
          open-on-workspace = "Comm";
        }
        {
          matches = [
            { app-id = "^org\.keepassxc\.KeePassXC$"; }
            { app-id = "^org\.gnome\.World\.Secrets$"; }
          ];
          block-out-from = "screencast";
        }
      ];
      binds = with config.lib.niri.actions; let
        sh = spawn "sh" "-c";
      in {
        "XF86AudioRaiseVolume" = {
          action.spawn = [ "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.05+" ];
          allow-when-locked = true;
        };
        "XF86AudioLowerVolume" = {
          action.spawn = [ "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.05-" ];
          allow-when-locked = true;
        };
        "XF86AudioMute" = {
          action.spawn = [ "wpctl" "set-mute" "@DEFAULT_AUDIO_SINK@" "toggle" ];
          allow-when-locked = true;
        };
        "XF86AudioMicMute" = {
          action.spawn = [ "wpctl" "set-mute" "@DEFAULT_AUDIO_SOURCE@" "toggle" ];
          allow-when-locked = true;
        };
        "XF86AudioPlay" = {
          action.spawn = [ "playerctl" "play-pause" ];
        };

        "XF86MonBrightnessUp".action = sh "brightnessctl set 10%+";
        "XF86MonBrightnessDown".action = sh "brightnessctl set 10%-";
        
        "Mod+Shift+Slash".action = show-hotkey-overlay;

        "Mod+T".action = spawn "ghostty";
        "Mod+Semicolon".action = spawn "cosmic-launcher";
        "Mod+Alt+Semicolon".action = spawn "cosmic-app-library";
        "Mod+L".action = spawn "loginctl" "lock-session";
        
        "Mod+Q".action = close-window;
        "Mod+Shift+E".action = quit;
        "Mod+Shift+P".action = power-off-monitors;

        "Mod+Left".action = focus-column-left;
        "Mod+Down".action = focus-window-or-workspace-down;
        "Mod+Up".action = focus-window-or-workspace-up;
        "Mod+Right".action = focus-column-right;

        "Mod+Ctrl+Left".action = move-column-left;
        "Mod+Ctrl+Down".action = move-window-down-or-to-workspace-down;
        "Mod+Ctrl+Up".action = move-window-up-or-to-workspace-up;
        "Mod+Ctrl+Right".action = move-column-right;

        "Mod+Home".action = focus-column-first;
        "Mod+End".action = focus-column-last;
        "Mod+Ctrl+Home".action = move-column-to-first;
        "Mod+Ctrl+End".action = move-column-to-last;

        "Mod+Shift+Left".action = focus-monitor-left;
        "Mod+Shift+Down".action = focus-monitor-down;
        "Mod+Shift+Up".action = focus-monitor-up;
        "Mod+Shift+Right".action = focus-monitor-right;
        "Mod+Shift+H".action = focus-monitor-left;
        "Mod+Shift+J".action = focus-monitor-down;
        "Mod+Shift+K".action = focus-monitor-up;
        "Mod+Shift+L".action = focus-monitor-right;

        "Mod+Shift+Ctrl+Left".action = move-column-to-monitor-left;
        "Mod+Shift+Ctrl+Down".action = move-column-to-monitor-down;
        "Mod+Shift+Ctrl+Up".action = move-column-to-monitor-up;
        "Mod+Shift+Ctrl+Right".action = move-column-to-monitor-right;
        "Mod+Shift+Ctrl+H".action = move-column-to-monitor-left;
        "Mod+Shift+Ctrl+J".action = move-column-to-monitor-down;
        "Mod+Shift+Ctrl+K".action = move-column-to-monitor-up;
        "Mod+Shift+Ctrl+L".action = move-column-to-monitor-right;

        "Mod+U".action = focus-workspace-down;
        "Mod+I".action = focus-workspace-up;
        "Mod+Ctrl+U".action = move-column-to-workspace-down;
        "Mod+Ctrl+I".action = move-column-to-workspace-up;

        "Mod+Shift+U".action = move-workspace-down;
        "Mod+Shift+I".action = move-workspace-up;
        "Mod+Alt+Ctrl+Right".action = move-workspace-to-monitor-right;
        "Mod+Alt+Ctrl+Left".action = move-workspace-to-monitor-left;

        "Mod+Shift+WheelScrollDown" = {
          action = focus-workspace-down;
          cooldown-ms = 150;
        };

        "Mod+Shift+WheelScrollUp" = {
          action = focus-workspace-up;
          cooldown-ms = 150;
        };

        "Mod+Ctrl+Shift+WheelScrollDown" = {
          action = move-column-to-workspace-down;
          cooldown-ms = 150;
        };

        "Mod+Ctrl+Shift+WheelScrollUp" = {
          action = move-column-to-workspace-up;
          cooldown-ms = 150;
        };

        "Mod+WheelScrollDown".action = focus-column-right;
        "Mod+WheelScrollUp".action = focus-column-left;
        "Mod+Ctrl+WheelScrollDown".action = move-column-right;
        "Mod+Ctrl+WheelScrollUp".action = move-column-left;

        "Mod+1".action = focus-workspace 1;
        "Mod+2".action = focus-workspace 2;
        "Mod+3".action = focus-workspace 3;
        "Mod+4".action = focus-workspace 4;
        "Mod+5".action = focus-workspace 5;
        "Mod+6".action = focus-workspace 6;
        "Mod+7".action = focus-workspace 7;
        "Mod+8".action = focus-workspace 8;
        "Mod+9".action = focus-workspace 9;
        "Mod+Ctrl+1".action = move-column-to-workspace 1;
        "Mod+Ctrl+2".action = move-column-to-workspace 2;
        "Mod+Ctrl+3".action = move-column-to-workspace 3;
        "Mod+Ctrl+4".action = move-column-to-workspace 4;
        "Mod+Ctrl+5".action = move-column-to-workspace 5;
        "Mod+Ctrl+6".action = move-column-to-workspace 6;
        "Mod+Ctrl+7".action = move-column-to-workspace 7;
        "Mod+Ctrl+8".action = move-column-to-workspace 8;
        "Mod+Ctrl+9".action = move-column-to-workspace 9;

        "Mod+Comma".action = consume-window-into-column;
        "Mod+Period".action = expel-window-from-column;

        "Mod+BracketLeft".action = consume-or-expel-window-left;
        "Mod+BracketRight".action = consume-or-expel-window-right;

        "Mod+R".action = switch-preset-column-width;
        "Mod+Shift+R".action = reset-window-height;
        "Mod+F".action = maximize-column;
        "Mod+Shift+F".action = fullscreen-window;
        "Mod+C".action = center-column;

        "Mod+KP_Subtract".action = set-column-width "-10%";
        "Mod+KP_Add".action = set-column-width "+10%";

        "Mod+Shift+KP_Subtract".action = set-window-height "-10%";
        "Mod+Shift+KP_Add".action = set-window-height "+10%";

        "Print".action = screenshot;
        "Ctrl+Print".action = screenshot-screen;
        "Alt+Print".action = screenshot-window;
      };
    };
  };

# Essential programs

  programs.foot = {
    enable = true;
    settings = {
      main = {
        font = "Monospace:size=11";
        pad = "1x1";
      };

      mouse = {
        hide-when-typing = "yes";
      };

      cursor = {
        color = "ffffff 009a8a";
      };
      colors = {
        background = "ffffff";
        foreground = "474747";

        regular0 = "ebebeb";
        regular1 = "d6000c";
        regular2 = "1d9700";
        regular3 = "c49700";
        regular4 = "0064e4";
        regular5 = "dd0f9d";
        regular6 = "00ad9c";
        regular7 = "878787";

        bright0 = "cdcdcd";
        bright1 = "bf0000";
        bright2 = "008400";
        bright3 = "af8500";
        bright4 = "0054cf";
        bright5 = "c7008b";
        bright6 = "009a8a";
        bright7 = "282828";
      };
    };
  };

  programs.ghostty = {
    enable = true;
    enableBashIntegration = true;
    enableFishIntegration = true;
    settings = {
      font-size = 11;
      font-family = [
        "Rec Mono Duotone"
      ];
      theme = "catppuccin-latte";
    };
  };


  programs.yazi = {
    enable = true;
    # theme = {
    #   light = "catppuccin-latte";
    # };
    # flavors = {
    #   catppuccin-latte = "${yazi-flavors}/catppuccin-latte.yazi";
    # };
  };
  
  programs.fzf.enable = true;

  programs.bat = {
    enable = true;
    config = {
      theme = "Solarized (light)";
    };
  };

  programs.starship = {
    enable = true;
    settings = {
      character = {
        success_symbol = "[λ](bold)";
        error_symbol = "[λ](bold red)";
      };
      package = {
        symbol = "";
      };
      time = {
        disabled = false;
      };
      nix_shell = {
        heuristic = true;
      };
    };
  };

  programs.bash = {
    enable = true;
    enableVteIntegration = true;
    initExtra = ''
      . "/etc/profiles/per-user/bill/etc/profile.d/hm-session-vars.sh"
    '';
  };

  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      fzf_configure_bindings # Restore default fish.fzf bindings
    '';
  };
  
  programs.ssh = {
    enable = true;
    matchBlocks = {
      "disco.usc.edu" = {
        identityFile = "~/.ssh/id_ed25519_usc";
        identitiesOnly = true;
        user = "billsun";
      };
      "hpc-ood.usc.edu" = {
        identityFile = "~/.ssh/id_ed25519_usc";
        identitiesOnly = true;
        proxyJump = "disco.usc.edu";
      };
    };
  };
  
  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };
  
  programs.git = {
    enable = true;
    difftastic.enable = true;
    userName = "Bill Sun";
    userEmail = "billksun@gmail.com"; 
    includes = [
      { 
        path = "~/Projects/USC/git-config.inc";
        condition = "gitdir:~/Projects/USC/";
      }
    ];
    extraConfig = {
      merge.tool = "kdiff3";
      init = {
        defaultBranch = "main";
      };
      pull = { 
        rebase = true;
      };
      push = {
        autoSetupRemote = true;
      };
    };
  };

  programs.command-not-found = {
    enable = true;
  };

  programs.bottom = {
    enable = true;
    settings = {
      flags = {
        # color = "default";
      };
    };
  };

  # Productivity

  # programs.thunderbird = {
  #   enable = true;
  # };

  programs.broot = {
    enable = true;
  };

  # programs.firefox = {
  #   enable = true;
  # };

  programs.helix = {
    enable = true;
    settings = {
      theme = "green-tea";
      editor = {
        whitespace.render = "all";
        lsp.display-messages = true;
        file-picker.hidden = false;
        true-color = true;
        color-modes = true;
        cursorline = true;
        soft-wrap.enable = true;
      };
      keys.normal = {
        C-e = "half_page_up";
      };
    };
    languages = {
      language-server = {
        haskell = {
          command = "haskell-language-server";
          args = ["--lsp"];
        };
        ltex = {
          command = "ltex-ls";
        };
        tailwindcss-language-server = {
          command = "tailwindcss-language-server";
          args = ["--stdio"];
          config = {
            userLanguages = { haskell = "html"; "*.hs" = "html"; };
          };
        };
      };
      language = [
        {
          name = "haskell";
          language-servers = [
            "haskell"
            "tailwindcss-language-server"
          ];
        }
        {
          name = "markdown";
          language-servers = [ 
            "ltex"
            "marksman"
          ];
        }
        {
          name = "html";
          language-servers = [ "tailwindcss-language-server" ];
        }
        {
          name = "css";
          language-servers = [ "tailwindcss-language-server" ];
        }
        {
          name = "jsx";
          language-servers = [ "tailwindcss-language-server" ];
        }
        {
          name = "tsx";
          language-servers = [ "tailwindcss-language-server" ];
        }
        {
          name = "svelte";
          language-servers = [ "tailwindcss-language-server" ];
        }
        {
          name = "org";
          language-servers = [
            "ltex"
          ];
        }
      ];
    };
  };
      
  programs.vscode = {
    enable = true;
    userSettings = {
      "workbench.colorTheme" = "Quiet Light";
      "files.autoSave" = "afterDelay";
      "window.zoomLevel" = 0;
      "window.titleBarStyle" = "native";
      "window.menuBarVisibility" = "toggle";
      "editor.fontLigatures" = true;
      "editor.fontFamily" = "'Monospace', 'Droid Sans Fallback'";
      "editor.formatOnType" = true;
      "editor.renderControlCharacters" = false;
      "git.autofetch" = true;
      "git.enableSmartCommit" = true;
      "http.proxySupport" = "override";
      "omnisharp.path" = "/home/bill/.nix-profile/bin/omnisharp";
      "omnisharp.useGlobalMono" = "never";
      "purescript.addSpagoSources" = true;
      "purescript.buildCommand" = "spago build -- --json-errors";
      "latex-workshop.view.pdf.viewer" = "tab";
      "markdown-preview-enhanced.latexEngine" = "tectonic";
      "cSpell.enabledLanguageIds" = [
        "asciidoc"
        "c"
        "cpp"
        "csharp"
        "css"
        "git-commit"
        "go"
        "handlebars"
        "haskell"
        "html"
        "jade"
        "java"
        "javascript"
        "javascriptreact"
        "json"
        "jsonc"
        "latex"
        "less"
        "markdown"
        "org"
        "php"
        "plaintext"
        "pug"
        "python"
        "restructuredtext"
        "rust"
        "scala"
        "scss"
        "text"
        "typescript"
        "typescriptreact"
        "yaml"
        "yml"
      ];
      "tailwindCSS.includeLanguages" = { 
        "haskell" = "html";
        "plaintext" = "html";
      };
      "angular.experimental-ivy" = true;
    };
    extensions = with pkgs.vscode-extensions; [
      ms-vsliveshare.vsliveshare
      ms-dotnettools.csharp
    ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace vscodeExtensionsFromMarketplace.extensions;
  };
      
  programs.obs-studio = {
    enable = true;
    plugins = with pkgs.obs-studio-plugins; [ 
      droidcam-obs
      obs-backgroundremoval
      obs-pipewire-audio-capture
      obs-gstreamer
      wlrobs
    ];
  };

  programs.mangohud = {
    enable = true;
  };
      
  services.syncthing = {
    enable = true;
    tray = {
      enable = true;
    };
  };
      
  services.mpris-proxy.enable = true;
  
  # Systemd customizations for loading syncthingtray in Plasma 5

  systemd.user.targets.tray = {
		Unit = {
			Description = "Home Manager System Tray";
			Requires = [ "graphical-session-pre.target" ];
		};
	};

  systemd.user.services.${config.services.syncthing.tray.package.pname} = {
    Install.WantedBy = lib.mkForce [ ];
  };
  
  systemd.user.timers.${config.services.syncthing.tray.package.pname} = {
    Timer = {
      OnActiveSec = "10s";
      AccuracySec = "1s";
    };
    Install = { WantedBy = [ "graphical-session.target" ]; };
  };
}
