# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

let
  json = pkgs.formats.json {};
  pwRnnoiseConfig = {
    "context.modules"= [
      { "name" = "libpipewire-module-filter-chain";
        "args" = {
          "node.description" = "Noise Cancelling Input";
          "media.name"       = "Noise Cancelling Input";
          "filter.graph" = {
            "nodes" = [
              {
                "type"   = "ladspa";
                "name"   = "rnnoise";
                "plugin" = "${pkgs.rnnoise-plugin}/lib/ladspa/librnnoise_ladspa.so";
                "label"  = "noise_suppressor_stereo";
                "control" = {
                  "VAD Threshold (%)" = 80.0;
                };
              }
            ];
          };
          "audio.position" = [ "FL" "FR" ];
          "capture.props" = {
            "node.name" = "effect_input.rnnoise";
            "node.passive" = true;
          };
          "playback.props" = {
            "node.name" = "effect_output.rnnoise";
            "media.class" = "Audio/Source";
          };
        };
      }
    ];
  };
  pwGameOneEQConfig = {
    "context.modules" = [
      { "name" = "libpipewire-module-filter-chain";
        "args" = {
          "node.description" = "Game One EQ Sink";
          "media.name" = "Game One EQ Sink";
          "filter.graph" = {
            "nodes" = [
                {
                  "type" = "builtin";
                  "name" = "eq_preamp";
                  "label" = "bq_highshelf";
                  "control" = { "Freq" = 0; "Q" = 1.0; "Gain" = -3.0; };
                }
                {
                  "type" = "builtin";
                  "name" = "convolver";
                  "label" = "convolver";
                  "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/results_oratory1990_harman_over-ear_2018_Sennheiser_GAME_ONE-48000Hz.wav"; };
                }
              ];
            "links" = [
              { "output" = "eq_preamp:Out"; "input" = "convolver:In"; }
            ];
            "inputs" = [ "eq_preamp:In" ];
            "outputs" = [ "convolver:Out" ];
          };
          "audio.channels" = 2;
          "audio.position" = [ "FL" "FR" ];
          "capture.props" = {
            "node.name" = "effect_input.gameone-convolution-eq";
            "media.class" = "Audio/Sink";
          };
          "playback.props" = {
            "node.name" = "effect_output.gameone-convolution-eq";
            "node.passive" = true;
          };
        };
      }
    ];
  };
  pwGameOneSurroundConfig = {
    "context.modules" = [
      { "name" = "libpipewire-module-filter-chain";
        "args" = {
          "node.description" = "Game One Surround Sink";
          "media.name" = "Game One Surround Sink";
          "filter.graph" = {
            "nodes" = [
                # {
                #   "type" = "builtin";
                #   "name" = "eq_preamp";
                #   "label" = "bq_highshelf";
                #   "control" = { "Freq" = 0; "Q" = 1.0; "Gain" = -6.6; };
                # }
                # {
                #   "type" = "builtin";
                #   "name" = "convolver";
                #   "label" = "convolver";
                #   "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/results_oratory1990_harman_over-ear_2018_Sennheiser_GAME_ONE-48000Hz.wav"; };
                # }
                
                # duplicate inputs
                { "type" = "builtin"; "label" = "copy"; "name" = "copyFL";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyFR";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyFC";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyRL";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyRR";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copySL";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copySR";  }
                { "type" = "builtin"; "label" = "copy"; "name" = "copyLFE"; }

                # apply hrir - HeSuVi 14-channel WAV (not the *-.wav variants) (note: */44/* in HeSuVi are the same, but resampled to 44100)
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFL_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  0; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFL_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  1; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convSL_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  2; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convSL_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  3; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convRL_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  4; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convRL_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  5; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFC_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  6; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFR_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  7; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFR_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  8; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convSR_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  9; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convSR_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 10; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convRR_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 11; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convRR_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 12; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convFC_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 13; }; }

                # treat LFE as FC
                { "type" = "builtin"; "label" = "convolver"; "name" = "convLFE_L"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" =  6; }; }
                { "type" = "builtin"; "label" = "convolver"; "name" = "convLFE_R"; "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/A3D.wav"; "channel" = 13; }; }

                # stereo output
                { "type" = "builtin"; "label" = "mixer"; "name" = "mixL"; }
                { "type" = "builtin"; "label" = "mixer"; "name" = "mixR"; }
              ];
            "links" = [
              # input
              { "output" = "copyFL:Out"; "input" = "convFL_L:In"; }
              { "output" = "copyFL:Out"; "input" = "convFL_R:In"; }
              { "output" = "copySL:Out"; "input" = "convSL_L:In"; }
              { "output" = "copySL:Out"; "input" = "convSL_R:In"; }
              { "output" = "copyRL:Out"; "input" = "convRL_L:In"; }
              { "output" = "copyRL:Out"; "input" = "convRL_R:In"; }
              { "output" = "copyFC:Out"; "input" = "convFC_L:In"; }
              { "output" = "copyFR:Out"; "input" = "convFR_R:In"; }
              { "output" = "copyFR:Out"; "input" = "convFR_L:In"; }
              { "output" = "copySR:Out"; "input" = "convSR_R:In"; }
              { "output" = "copySR:Out"; "input" = "convSR_L:In"; }
              { "output" = "copyRR:Out"; "input" = "convRR_R:In"; }
              { "output" = "copyRR:Out"; "input" = "convRR_L:In"; }
              { "output" = "copyFC:Out"; "input" = "convFC_R:In"; }
              { "output" = "copyLFE:Out"; "input" = "convLFE_L:In"; }
              { "output" = "copyLFE:Out"; "input" = "convLFE_R:In"; }

              # output
              { "output" = "convFL_L:Out"; "input" = "mixL:In 1"; }
              { "output" = "convFL_R:Out"; "input" = "mixR:In 1"; }
              { "output" = "convSL_L:Out"; "input" = "mixL:In 2"; }
              { "output" = "convSL_R:Out"; "input" = "mixR:In 2"; }
              { "output" = "convRL_L:Out"; "input" = "mixL:In 3"; }
              { "output" = "convRL_R:Out"; "input" = "mixR:In 3"; }
              { "output" = "convFC_L:Out"; "input" = "mixL:In 4"; }
              { "output" = "convFC_R:Out"; "input" = "mixR:In 4"; }
              { "output" = "convFR_R:Out"; "input" = "mixR:In 5"; }
              { "output" = "convFR_L:Out"; "input" = "mixL:In 5"; }
              { "output" = "convSR_R:Out"; "input" = "mixR:In 6"; }
              { "output" = "convSR_L:Out"; "input" = "mixL:In 6"; }
              { "output" = "convRR_R:Out"; "input" = "mixR:In 7"; }
              { "output" = "convRR_L:Out"; "input" = "mixL:In 7"; }
              { "output" = "convLFE_R:Out"; "input" = "mixR:In 8"; }
              { "output" = "convLFE_L:Out"; "input" = "mixL:In 8"; }

              # EQ
              # { "output" = "eq_preamp:Out"; "input" = "convolver:In"; }
            ];
            # "inputs" = [ "eq_preamp:In" ];
            # "outputs" = [ "convolver:Out" ];
            "inputs"  = [ "copyFL:In" "copyFR:In" "copyFC:In" "copyLFE:In" "copyRL:In" "copyRR:In" "copySL:In" "copySR:In" ];
            "outputs" = [ "mixL:Out" "mixR:Out" ];
          };
          "capture.props" = {
            "node.name" = "effect_input.gameone-surround";
            "media.class" = "Audio/Sink";
            "audio.channels" = 8;
            "audio.position" = [ "FL" "FR" "FC" "LFE" "RL" "RR" "SL" "SR" ];
          };
          "playback.props" = {
            "node.name" = "effect_output.gameone-surround";
            "node.passive" = true;
            "audio.channels" = 2;
            "audio.position" = [ "FL" "FR" ];
          };
        };
      }
    ];
  };
pwVirtualSurroundConfig = {
  "context.modules" = [
    { "name" = "libpipewire-module-filter-chain";
      "args" = {
        "node.description" = "Virtual Surround Sink";
        "media.name" = "Virtual Surround Sink";
        "filter.graph" = {
          "nodes" = [
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spFL";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 30.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spFR";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 330.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spFC";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 0.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spRL";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 150.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spRR";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 210.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spSL";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 90.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spSR";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 270.0; "Elevation" = 0.0; "Radius" = 3.0; };
            }
            {
              "type" = "sofa";
              "label" = "spatializer";
              "name" = "spLFE";
              "config" = { "filename" = "/home/bill/.config/pipewire/pipewire.conf.d/hrtf_b_nh724.sofa"; };
              "control" = { "Azimuth" = 0.0; "Elevation" = -6.0; "Radius" = 3.0; };
            }

            # Downmix to 2 channels
            { "type" = "builtin"; "label" = "mixer"; "name" = "mixL"; }
            { "type" = "builtin"; "label" = "mixer"; "name" = "mixR"; }
          ];
          links = [
            # output
            { "output" = "spFL:Out L"; "input" = "mixL:In 1"; }
            { "output" = "spFL:Out R"; "input" = "mixR:In 1"; }
            { "output" = "spFR:Out L"; "input" = "mixL:In 2"; }
            { "output" = "spFR:Out R"; "input" = "mixR:In 2"; }
            { "output" = "spFC:Out L"; "input" = "mixL:In 3"; }
            { "output" = "spFC:Out R"; "input" = "mixR:In 3"; }
            { "output" = "spRL:Out L"; "input" = "mixL:In 4"; }
            { "output" = "spRL:Out R"; "input" = "mixR:In 4"; }
            { "output" = "spRR:Out L"; "input" = "mixL:In 5"; }
            { "output" = "spRR:Out R"; "input" = "mixR:In 5"; }
            { "output" = "spSL:Out L"; "input" = "mixL:In 6"; }
            { "output" = "spSL:Out R"; "input" = "mixR:In 6"; }
            { "output" = "spSR:Out L"; "input" = "mixL:In 7"; }
            { "output" = "spSR:Out R"; "input" = "mixR:In 7"; }
            { "output" = "spLFE:Out L"; "input" = "mixL:In 8"; }
            { "output" = "spLFE:Out R"; "input" = "mixR:In 8"; }
          ];
          "inputs" = [ "spFL:In" "spFR:In" "spFC:In" "spLFE:In" "spRL:In" "spRR:In" "spSL:In" "spSR:In" ];
          "outputs" = [ "mixL:Out" "mixR:Out" ];
        };
        "capture.props" = {
          "node.name" = "effect_input.spatializer";
          "media.class" = "Audio/Sink";
          "audio.channels" = 8;
          "audio.position" = [ "FL" "FR" "FC" "LFE" "RL" "RR" "SL" "SR" ];
        };
        "playback.props" = {
          "node.name" = "effect_output.spatializer";
          "node.passive" = "true";
          "audio.channels" = 2;
          "audio.position" = [ "FL" "FR" ];
        };
      };
    }
  ];
};
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Hardware configuration
  hardware.sane.enable = true;
  hardware.i2c.enable = true;
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
    ];
  };
  
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # Linux kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.extraModulePackages = [ config.boot.kernelPackages.v4l2loopback ];

  # Virtualization
  virtualisation = {
    libvirtd.enable = true;
    podman = {
      enable = true;
      defaultNetwork.settings.dns_enabled = true;
    };
  };

  networking.hostName = "usc-hp-elitebook-x360-1040-g8"; # Define your hostname.
  networking.networkmanager.enable = true;
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  # networking.useDHCP = false;
  # networking.interfaces.enp4s0.useDHCP = true;
  # networking.interfaces.wlp5s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Configuration to enable dnscrypts-proxy2
  # networking.nameservers = [ "127.0.0.1" "::1" ];
  # networking.resolvconf.enable = false;
  # networking.dhcpcd.extraConfig = "nohook resolve.conf";
  # networking.networkmanager.dns = "none";

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_US.UTF-8";
    inputMethod = {
      enabled = "fcitx5";
      fcitx5 = {
        waylandFrontend = true;
        plasma6Support = config.services.desktopManager.plasma6.enable;
        addons = [ pkgs.fcitx5-rime ];
      };
    };
  };
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Enable the Plasma 6 Desktop Environment.
  services.xserver.enable = true;
  services.xserver.dpi = 109;
  # services.xserver.displayManager.sddm.enable = true;
  services.desktopManager.plasma6 = {
    enable = true;
    enableQt5Integration = true;
  };
  services.desktopManager.cosmic.enable = true;
  services.displayManager.cosmic-greeter.enable = true;
  programs.niri = {
    enable = true;
    package = pkgs.niri;
  };
  # services.xserver.videoDrivers = [ "amdgpu-pro" ];
  # hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.legacy_390;
  hardware.opengl.driSupport32Bit = true;
  security.pam.services.kdewallet.enableKwallet = true;

  # Enable XMonad Windows Manager.
  services.xserver.windowManager = {
    xmonad.enable = true;
    xmonad.enableContribAndExtras = true;
    xmonad.extraPackages = haskellPackages: [
      haskellPackages.split
    ];
  };
    
  # Configure fonts
  fonts = {
    packages = with pkgs; [
      nerdfonts
      sudo-font
      mplus-outline-fonts.githubRelease
      google-fonts
      source-sans
      source-han-sans
      source-serif
      source-han-serif
      lmodern
      league-of-moveable-type
      recursive
    ];

    fontconfig.defaultFonts = {
      monospace = lib.mkForce [ "Rec Mono Duotone" "Noto Sans Mono CJK TC" " Noto Sans Mono CJK SC" ];
      sansSerif = lib.mkForce [ "Source Sans Pro" "Source Han Sans TC" "Source Han Sans SC" ];
      serif     = lib.mkForce [ "Source Serif Pro" "Source Han Serif TC" "Source Han Serif SC" ];
    };
  };
  
  # Configure keymap in X11
  services.xserver.xkb.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = with pkgs; [
      gutenprint
      gutenprintBin
      foomatic-filters
    ];
  };

  # Enable Bluetooth
  hardware.bluetooth = {
    enable = true;
    # settings = {
    #   general = {
    #     enable = "Source,Sink,Media,Socket";
    #   };
    # };
  };

  # Enable sound.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
    extraConfig = {
      pipewire = {
        # Generate PipeWire noise suppression config
        "99-input-rnnoise.conf" = pwRnnoiseConfig;

        # Generate PipeWire Game One EQ config
        "100-sink-gameone-eq.conf" = pwGameOneEQConfig;

        # Generate PipeWire Game One virtual surround config
        "101-sink-gameone-surround.conf" = pwGameOneSurroundConfig;

        # Generate PipeWire virtual surround config
        "102-sink-surround.conf" = pwVirtualSurroundConfig;
      };
    };
  };

  # Enable monitor brightness control
  services.ddccontrol.enable = true;

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    defaultUserShell = pkgs.fish;
    users.bill = {
      description = "Bill Kuo-Shin Sun";
      isNormalUser = true;
      extraGroups = [
        "wheel"
        "networkmanager"
        "syncthing"
        "scanner"
        "lp"
        "kvm"
        "libvirtd"
        "adbusers"
      ];
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [ "electron-24.8.6" "electron-22.3.27" ];
  environment.systemPackages = with pkgs; [
    wl-clipboard
    wget
    ntfs3g
    home-manager
    lm_sensors
    hdparm
    fzf
    fd
    bat
    fishPlugins.fzf-fish
    fishPlugins.done
    podman-compose
  ];
  
  # Environment
  environment.sessionVariables = {
    # The following variables are set to make fcitx input method switching work in wayland
    NIX_PROFILES =
      "${lib.strings.concatStringsSep " " (lib.lists.reverseList config.environment.profiles)}";
    XMODIFIERS = "@im=fcitx";
  };
  
  # Nix configuration.
  nix = {
    settings = {
      substituters= [
        "https://cache.nixos.org" "https://nixcache.reflex-frp.org"
        "https://cache.iog.io"
      ];
      trusted-public-keys = [
        "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="
        "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
      ];
      trusted-users = [ "root" "bill" ];
    };    
    gc.automatic = true;
    gc.options = "--delete-older-than 30d";
    extraOptions = ''
      auto-optimise-store = true
      keep-outputs = true
      experimental-features = nix-command flakes
    '';
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  programs.fish.enable = true;
  programs.firefox.enable = true;
  programs.ssh.startAgent = true;
  programs.partition-manager.enable = true;
  programs.kdeconnect.enable = true;
  programs.dconf.enable = true; # For easyeffects
  # programs.adb.enable = true;
  programs.corectrl = {
    enable = true;
    gpuOverclock.enable = true;
  };
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    gamescopeSession.enable = true;
  };
  programs.weylus = {
    enable = true;
    openFirewall = true;
  };
  programs.singularity.enable = true;

  # List services that you want to enable:
  
  # Enable one-time service to create btrfs swap file
  # systemd.services = {
  #   create-swapfile = {
  #     serviceConfig.Type = "oneshot";
  #     wantedBy = [ "swap-swapfile.swap" ];
  #     script = ''
  #       ${pkgs.coreutils}/bin/truncate -s 0 /swap/swapfile
  #       ${pkgs.e2fsprogs}/bin/chattr +C /swap/swapfile
  #       ${pkgs.btrfs-progs}/bin/btrfs property set /swap/swapfile compression none
  #     '';
  #   };
  # };

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Enable dnscrypt-proxy2
  # services.dnscrypt-proxy2 = {
  #   enable = true;
  # };

  services.fwupd.enable = true;

  # Enable Avahi
  services.avahi = {
    enable = true;
    nssmdns4 = true;
  };

  # Enable flatpak
  services.flatpak.enable = true;
  xdg.portal = {
    enable = true;
    wlr.enable = true;
  };

  # Enable backup
  services.restic.backups = {
    local = {
      initialize = true;
      paths = [ "/home/" "/mnt/wd-1tb/Videos/" ];
      repository = "/mnt/hgst-6tb-2017/Backups/";
      passwordFile = "/etc/nixos/secrets/local-backup";
      pruneOpts = [
        "--keep-daily 14"
        "--keep-weekly 13"
      ];
    };
    # remote = {
    #   initialize = true;
    #   paths = [ "/home/" ];
    #   repository = "rclone:remote-b2:backup-bill/Repository";
    #   passwordFile = "/etc/nixos/secrets/remote-backup";
    #   rcloneConfigFile = "/home/bill/.config/rclone/rclone.conf";
    #   timerConfig = {
    #     OnCalendar = "weekly";
    #   };
    #   pruneOpts = [
    #     "--keep-daily 14"
    #     "--keep-weekly 13"
    #   ];
    # };
  };

  # Enable OpenRGB
  services.hardware.openrgb = {
    enable = true;
    motherboard = "amd";
  };

  # Enable extra udev rules for Atreus
  services.udev.packages = [ pkgs.chrysalis ];

  # services.slurm = {
  #   server.enable = true;
  #   client.enable = true;
  #   nodeName = [
  #     "usc-hp-elitebook-x360-1040-g8 CPUs=1 State=UNKNOWN"
  #   ];
  #   partitionName = [
  #     "main Nodes=usc-hp-elitebook-x360-1040-g8 Default=YES MaxTime=INFINITE State=UP"
  #   ];
  #   extraConfig = ''
  #     SlurmctldHost=usc-hp-elitebook-x360-1040-g8

  #     AuthAltTypes=auth/jwt
  #     AuthAltParameters=jwt_key=/var/spool/slurmctld/jwt_hs256.key
  #     AccountingStorageType=accounting_storage/slurmdbd
  #   '';
  #   dbdserver = {
  #     enable = true;
  #     storagePassFile = "/var/spool/slurmctld/storage_pass";
  #     extraConfig = ''
  #       AuthAltTypes=auth/jwt
  #       AuthAltParameters=jwt_key=/var/spool/slurmctld/jwt_hs256.key
  #     '';
  #   };
  # };
  # services.mysql = {
  #   enable = true;
  #   package = pkgs.mariadb;
  #   ensureUsers = 
  #   [
  #     {
  #       name = "bill";
  #       ensurePermissions = {
  #         "*.*" = "ALL PRIVILEGES";
  #       };
  #     }
  #     {
  #       name = "slurm";
  #       ensurePermissions = {
  #         "slurm_acct_db.*" = "ALL PRIVILEGES";
  #       };
  #     }
  #   ];
  #   ensureDatabases = [ "slurm_acct_db" ];
  # };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 5900 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  services.tailscale = {
    enable = true;
    authKeyFile = "/home/bill/.config/tailscale/tailscale.key";
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?
}

